all:
	pdflatex relatorio.tex && bibtex relatorio && pdflatex relatorio.tex && pdflatex relatorio.tex 

clean:
	rm -rf *.aux *.log *.swp *.out _minted* *.gz *.fls *.fdb_latexmk *.toc *.idx *.lof *.lot *.bbl *.blg *.brf *.dvi
