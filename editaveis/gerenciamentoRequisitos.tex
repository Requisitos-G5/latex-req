\chapter[Estratégia de Rastreabilidade]{Estratégia de Rastreabilidade}

  Segundo o RUP, a rastreabilidade é a capacidade de rastrear um elemento do projeto com outros elementos correlatos, especialmente aqueles relacionados a algum requisito. Sendo assim, é um conceito que facilita a compreensão e auxilia a gerência de requisitos, pois fornece uma assistência fundamental na relação e controle dos requisitos, elementos de modelagem e outros artefatos do processo de \emph{software}.

  Por meio da rastreabilidade de requisitos é possível obter a origem de cada elemento que faz parte do processo, bem como os requisitos que estão relacionados a estes elementos e quais deles serão afetados quando houver algum tipo de mudança, oferecendo assim, uma possível análise de impactos causados em decorrência dessas alterações.

  Um requisito é rastreável se podemos descobrir sua origem, a questão é como identificá-los de maneira natural. Logo se propõe a utilização de um \emph{check-list} com as seguintes perguntas:

  \begin{itemize}
    \item Por que ele existe?
    \item Com quais requisitos ele está ligado?
    \item Como o requisito está ligado ao produto?
    \item Quais características do produto são definidas pelo requisito?
  \end{itemize}

  A rastreabilidade também pode ser classificada por rastreabilidade horizontal, vertical, pré-rastreabilidade e pós-rastreabilidade, além disso existem duas capacidades que devem estar presentes em todos esses tipos de rastreabilidade, servindo como uma propriedade básica para essa atividade. São elas: a capacidade de rastrear um requisito até seus refinamentos, definida como rastreabilidade para frente (\emph{forwards}); e a de rastrear um refinamento até sua origem, que é definida como rastreabilidade para trás (\emph{backwards}).~\cite{davis}

  A rastreabilidade horizontal  é a rastreabilidade entre diferentes variações de requisitos, ou outros artefatos em um ciclo de vida. Enquanto que a rastreabilidade vertical é realizada entre requisitos e artefatos produzidos pelo processo de desenvolvimento ao longo do ciclo de vida do projeto.~\cite{gotel}

  \section{Estratégia de rastreabilidade}

    Os elos da rastreabilidade foram definidos de acordo com o processo que foi especificado e modelado para o nosso trabalho, sendo definido aspectos dos itens de rastreabilidade como, o \textbf{problema}, a \textbf{necessidade}, as \textbf{características} e os \textbf{requisitos} e como os mesmos iram se relacionar entre si e em nível de dependência, visando alcançar uma boa rastreabilidade de requisitos para o processo.

    Tendo como referência o modelo do processo, os conceitos de rastreabilidade, e tendo em plano o que foi definido para caracterizar essa rastreabilidade, foi definido que a rastreabilidade poderia ser tradada com a técnica da matriz de rastreabilidade de requisitos que, segundo o PMBOK \cite{pmbok}, fornece uma estrutura de gerenciamento das mudanças do escopo do produto, além de ser uma funcionalidade existente na ferramenta escolhida para o gerenciamento dos requisitos.

    Dado que a rastreabilidade de um requisito é utilizada para prover relacionamento entre os requisitos e outros aspectos inseridos no contexto do processo, ela também possibilita uma adequada compreensão dos relacionamentos de dependência entre os requisitos, e, atravês dos artefatos de requisitos, foi levantado os relacionamentos que seriam levados em consideração dentro das matrizes de rastreabilidade de requisitos. São eles:

    \begin{itemize}
      \item Dependência entre requisitos
      \item Relação de requisitos com caracteristicas
      \item Relação das características com as necessidades do cliente
    \end{itemize}

    Sabendo que as necessidades do cliente refletem diretamente na busca de solucções para um problema ou para alcançar um determinado objetivo, não foi levada em consideração a relação das necessidades do cliente com os problemas a serem solucionados.

    A representação dessas relações estão presentes na figura \ref{fig:necessidades}.

    \begin{figure}[H]
      \center
      \includegraphics[width=0.3\textwidth]{figuras/necessidades.png}
      \caption{Relação entre requisitos}
      \label{fig:necessidades}
    \end{figure}

    Para representar os itens de rastreabilidade que estarão presentes nas matrizes de rastreabilidade de requisitos foram definidos \emph{TAGs}, que seriam abreviaturas dos nomes dos  aspectos atribuídos a esses itens, são elas:

    \begin{itemize}
      \item \textbf{Requisito funcional (RF)}: Define uma função de um sistema
      \item \textbf{Requisito não-funcional (RNF)}: Características e aspectos internos do sistema
      \item \textbf{Característica (CH)}: São referentes as funcionalidades do sistema
      \item \textbf{Necessidades (NC)}: São as necessidades levantadas pelo cliente
    \end{itemize}

  \section{Dependência entre requisitos}

    A tabela \ref{tab:rastreabilidadeReq} mostra um exemplo de matriz de rastreabilidade onde os requisitos da coluna a extrema esquerda são dependentes dos requisitos da linha superior, por exemplo: com a alteração no Requisito 3 deverá ser avaliado os impactos nas mudanças dos requisitos 1 e 2. Essa tabela é valida para todos os tipos de requisitos, sendo que estes tipo podem ser relacionados nessa tabela.

    \begin{table}[H]
      \centering
        \begin{tabular}{c|ccccc}
          & RF01 & RF02 & RF03 & RF04 & \ldots \\
          \hline
          RF01 & ----- & & & & \ldots \\
          RF02 & X & ----- & & & \ldots \\
          RF03 & X & X & ----- & & \ldots \\
          RF04 & X & & & ----- & \ldots \\
          \ldots & \ldots & \ldots & \ldots & \ldots & \ldots \\
        \end{tabular}
      \caption{Matriz de rastreabilidade entre requisitos}
      \label{tab:rastreabilidadeReq}
    \end{table}

    As TAGs referidas que podem constar na tabela são:
    \begin{itemize}
      \item \textbf{RF}: Requisito funcional
      \item \textbf{RNF}: Requisito não-funcional
    \end{itemize}

  \section{Relação de requisitos com caracteristicas}

    A tabela \ref{tab:rastreabilidadeReqCH} mostra um exemplo de matriz de rastreabilidade onde os requisitos da coluna a extrema esquerda estão relacionados as suas caracteristicas, especificadas na linha superior. Caracteristicas nas quais são relacionadas as necessidades do cliente, o que torna uma rastreabilidade indireta entre os requisitos e as necessidades do cliente.

    \begin{table}[H]
      \centering
        \begin{tabular}{c|ccccc}
          & CH01 & CH02 & CH03 & CH04 & \ldots \\
          \hline
          RF01 & X & & & & \ldots \\
          RF02 & X & X & & & \ldots \\
          RF03 & & X & X & & \ldots \\
          RF04 & X & & & X & \ldots \\
          \ldots & \ldots & \ldots & \ldots & \ldots & \ldots \\
        \end{tabular}
      \caption{Matriz de rastreabilidade entre requisitos e caracteristicas}
      \label{tab:rastreabilidadeReqCH}
    \end{table}

    As \emph{TAGs} referidas que podem constar na tabela são:
    \begin{itemize}
      \item \textbf{RF}: Requisito funcional
      \item \textbf{RNF}: Requisito não-funcional
      \item \textbf{CH}: Característica
    \end{itemize}

  \section{Relação das características com as necessidades do cliente}

    A tabela \ref{tab:rastreabilidadeCHNC} mostra um exemplo de matriz de rastreabilidade onde as caracteristicas da coluna a extrema esquerda correspondem as necessidades do cliente especificadas na linha superior. Uma necessidade do cliente possui uma ou mais caracteísticas do sistema, e uma dessas caracteristicas corresponde a uma necessidade do cliente.

    \begin{table}[H]
      \centering
        \begin{tabular}{c|ccccc}
          & CN01 & NC02 & NC03 & NC04 & \ldots \\
          \hline
          CH01 & X & & & & \ldots \\
          CH02 & & X & & & \ldots \\
          CH03 & & & X & & \ldots \\
          CH04 & & X & & X & \ldots \\
          \ldots & \ldots & \ldots & \ldots & \ldots & \ldots \\
        \end{tabular}
      \caption{Matriz de rastreabilidade entre requisitos e caracteristicas}
      \label{tab:rastreabilidadeCHNC}
    \end{table}

    As TAGs referidas que podem constar na tabela são:
    \begin{itemize}
      \item \textbf{CH}: Característica
      \item \textbf{NC}: Necessidade
    \end{itemize}

  \section{Atributos de requisitos}

    Atributos são uma fonte muito importante de informações sobre requisitos. Atributos fazem mais do que simplesmente clarificar um requisito. Se criados apropriadamente, eles podem fornecer informações significantes sobre o estado do sistema. Eles facilitam a consulta, por exemplo, de requisitos que foram cadastrados nos últimos 30 dias (atributo de data de criação), e dessa forma mostram o status destes atributos selecionandos para o cliente.

    Na seção \ref{sec:ferramenta-escolhida} discutimos qual a ferramenta que utilizaremos, e dessa forma devemos entender quais atributos ela possibilita cadastrar. A ferramenta escolhida é a Innoslate, e esses são os atributos que ela disponibiliza:

    \begin{enumerate}
      \item \textbf{\emph{Text}}: Uma linha de caracteres.
      \item \textbf{\emph{Number}}: Qualquer número real individual.
      \item \textbf{\emph{Boolean}}: Os dois valores lógicos possíveis: Verdadeiro ou Falso.
      \item \textbf{\emph{Percent}}: Valor limitado entre 0 e 100.
      \item \textbf{URI}: Caso especial de texto onde o valor deve ser um \emph{Uniform Resource Identifier}
      \item \textbf{\emph{DateTime}}: Methodo específico de armazenamoento de datas
      \item \textbf{\emph{Enumeration}}: Conjunto de escolhas, mas uma só escolha pode ser selecionada.
      \item \textbf{\emph{GeoPoint}}: Latitude e longitude para localização.
    \end{enumerate}

    Além desses tipos de atributos definidos pela LML Specification 1.0 \cite{lml2013}, o Innoslate adiciona os seguintes tipos:

    \begin{enumerate}
      \item \textbf{\emph{Big Text}}: Muitas linhas de texto.
      \item \textbf{\emph{File}}: Arquivo para upload.
      \item \textbf{\emph{Duration}}: Um método de guardar duração em termos de datas.
      \item \textbf{\emph{Quality}}: Similar ao Boolean, mas para comparação entre requisitos.
      \item \textbf{\emph{Multiplicity}}: Um número, ou um intervalo.
    \end{enumerate}

    Dessa forma teremos os seguintes atributos:

    \begin{enumerate}
      \item \textbf{Nome}: Será do tipo \emph{Text} e irá guardar o nome do requisito.
      \item \textbf{Descrição}: Será do tipo \emph{Big Text} e irá explicar o requisito.
      \item \textbf{Data de criação}: Será do tipo \emph{DateTime} e guardará a data em que o requisito foi criado.
      \item \textbf{Data de início}: Será do tipo \emph{DateTime} e guardará a data em que o requisito começa.
      \item \textbf{Data de encerramento}: Será do tipo \emph{DateTime} e guardará a data em que o requisito acaba.
      \item \textbf{Duração}: Será do tipo \emph{DateTime} e guardará a duração do requisito.
      \item \textbf{Progresso}: Será do tipo \emph{Percent} e guardará o progresso do requisito.
      \item \textbf{URI}: Será também do tipo URI e será o atributo de indentificação.
    \end{enumerate}

    Outros atributos poderão ser criados de acordo com a necessidade individual de cada requisito, mas os atributos acima serão obrigatórios à todos os requisitos.
